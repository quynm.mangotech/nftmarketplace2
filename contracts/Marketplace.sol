// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract Marketplace is ERC721URIStorage{
    using Counters for Counters.Counter;
    Counters.Counter private _itemIds;
    address payable owner;

    constructor() ERC721("MYNFT", "NFT"){
        owner = payable(msg.sender);
    }

    struct Item {
        uint itemId;
        address payable owner;
        uint256 price;
        bool onSale;
    }

    mapping(uint256 => Item) private MarketItem_Ids;

    // Create token and list to marketplace
    function createToken(string memory tokenURI, uint price) external payable returns(uint256) {
        _itemIds.increment();
        uint256 newItemId = _itemIds.current();
        _safeMint(msg.sender, newItemId);
        _setTokenURI(newItemId, tokenURI);
        setApprovalForAll(address(this), true);
        mintNFT(newItemId, price);
        return newItemId;
    }

    // Add item to the marketplace
    function mintNFT(uint256 _itemId, uint _price) private returns(Item memory){
        require(_price > 0, "Price must be at least 1 wei");
        Item memory item = Item(_itemId, payable(msg.sender), _price, true);
        MarketItem_Ids[_itemId] = item;
        return item;
    }

    // Buy NFT
    function buyNFT( uint256 _itemId) external payable {
        uint256 price = MarketItem_Ids[_itemId].price;
        uint256 itemId = MarketItem_Ids[_itemId].itemId;
        require(msg.value == price, "You must pay the price of the item");
        MarketItem_Ids[_itemId].owner.transfer(msg.value - msg.value/10);
        IERC721(address(this)).safeTransferFrom(MarketItem_Ids[_itemId].owner, msg.sender, itemId);
        MarketItem_Ids[_itemId].owner = payable(msg.sender);
        MarketItem_Ids[_itemId].onSale = false;
        owner.transfer(msg.value/10);
    }

    // For sale my item
    function forSale(uint itemId) external {
        require(MarketItem_Ids[itemId].owner == msg.sender,  "only owner has privilege to sell this item");
            MarketItem_Ids[itemId].onSale = true;
    }   

    // Fetch list of items for sale
    function fetchNFTs() external view returns (Item[] memory) {
        uint256 itemCount = _itemIds.current();
        uint256 currentIndex = 0;
        Item[] memory items = new Item[] (itemCount);
        for (uint256 i = 1; i< itemCount+1; i++) {
            if(MarketItem_Ids[i].onSale == true) {
                items[currentIndex] = MarketItem_Ids[i];
                currentIndex++;
            }
        }
        return items;
    }   

    // Fetch my list of items
    function fetchMyNfts() external view returns (Item[] memory) {
        uint256 itemCount = 0;
        uint256 currentIndex = 0;
        uint256 totalItems =_itemIds.current();
        for (uint256 i = 1; i <= totalItems; i++) {
            if(MarketItem_Ids[i].owner == msg.sender) {
                itemCount += 1;
            }  
        }
        Item[] memory items = new Item[](itemCount);
        for (uint i =1; i<= totalItems; i++) {
            if(MarketItem_Ids[i].owner == msg.sender) {
                items[currentIndex] = MarketItem_Ids[i];
                currentIndex++;
            }
        }
        return items;
    }
}