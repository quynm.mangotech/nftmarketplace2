const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Marketplace", function () {
  it("Should create and execute marketplace sales", async function () {
    const Marketplace = await ethers.getContractFactory("Marketplace");
    const marketplace = await Marketplace.deploy();
    const mContract = await marketplace.deployed();

    await mContract.createToken("https://www.mytoken1location.com", ethers.utils.parseEther(String(10)));
    await mContract.createToken("https://www.mytoken2location.com", ethers.utils.parseEther(String(50)));
    await mContract.createToken("https://www.mytoken3location.com", ethers.utils.parseEther(String(80)));

    const [_, buyerAddress] = await ethers.getSigners();
   
    await mContract.connect(buyerAddress).buyNFT( 1, {value: ethers.utils.parseEther(String(10))});

    const items = await mContract.fetchNFTs();

    console.log(mContract)

    expect(items.length).to.equal(3);

  });
});
